//Main.cpp

#include "stdafx.h"

struct StructNodeBase
{
	int value;
	StructNodeBase* Nextnode;
};

StructNodeBase* SHeadNode = new StructNodeBase;
StructNodeBase* SCurrNode = new StructNodeBase;
StructNodeBase* STempNode = new StructNodeBase;

void Push_Front(int value)
{
	StructNodeBase* SNewNode = new StructNodeBase;

	SNewNode->value = value;
	SNewNode->Nextnode = NULL;

	if (SHeadNode->value != NULL)
	{
		SNewNode->Nextnode = SHeadNode;
		SHeadNode = SNewNode;
	}
	else
	{
		SHeadNode = SNewNode;
		SHeadNode->Nextnode = nullptr;
	}
}

void Push_Back(int value)
{
	StructNodeBase* SNewNode = new StructNodeBase;
	SNewNode->value = value;
	SNewNode->Nextnode = NULL;

	SCurrNode = SHeadNode;

	while (SCurrNode->Nextnode != NULL)
	{
		SCurrNode = SCurrNode->Nextnode;
	}

	if (SHeadNode->value == NULL)
	{
		SHeadNode = SNewNode;
	}
	else
	{
		SCurrNode->Nextnode = SNewNode;
	}

}

void Pop_Front()
{
	STempNode = SHeadNode;
	SHeadNode = STempNode->Nextnode;
	delete STempNode;
}

void Pop_Back()
{
	SCurrNode = SHeadNode;

	if (SHeadNode->Nextnode == nullptr)
	{
		SHeadNode = nullptr;
		return;
	}

	while (SCurrNode->Nextnode->Nextnode != nullptr)
	{
		SCurrNode = SCurrNode->Nextnode;
	}

	delete SCurrNode->Nextnode;
	SCurrNode->Nextnode = nullptr;
}

int GetSize()
{
	SCurrNode = SHeadNode;
	int SizeValue = 0;

	while (SCurrNode != NULL)
	{
		SCurrNode = SCurrNode->Nextnode;
		SizeValue++;
	}
	return SizeValue;
}
void ClearNodes()
{
	SCurrNode = SHeadNode;
	STempNode = SCurrNode;

	while (SCurrNode != nullptr)
	{
		STempNode = SCurrNode;
		SCurrNode = SCurrNode->Nextnode;

		delete STempNode;
		STempNode = nullptr;
	}
	SHeadNode = nullptr;
}

int FindNodeWithPosition(int value)
{
	bool state = true;
	SCurrNode = SHeadNode;
	int PositionValue = 1;

	while (state)
	{
		if (PositionValue < value)
		{
			if (SCurrNode->Nextnode == NULL)
			{
				return INT_MAX;
			}
			SCurrNode = SCurrNode->Nextnode;
			PositionValue++;
		}
		else
		{
			return SCurrNode->value;
		}
	}
	return 0;
}

void UniTest()
{
	std::cout << "Running test" << std::endl;

	Push_Front(2);
	std::cout << "Expecting to push front value 2, value pushed: ";
	int Vtemp = FindNodeWithPosition(1);
	std::cout << Vtemp << std::endl;

	Push_Back(3);
	std::cout << "Expecting to push back value 3, value pushed: ";
	Vtemp = FindNodeWithPosition(2);
	std::cout << Vtemp << std::endl;

	Push_Front(1);
	std::cout << "Expecting to push front value 1, value pushed: ";
	Vtemp = FindNodeWithPosition(1);
	std::cout << Vtemp << std::endl;

	Push_Back(4);
	std::cout << "Expecting to push back value 4, value pushed: ";
	Vtemp = FindNodeWithPosition(4);
	std::cout << Vtemp << std::endl;

	Vtemp = GetSize();
	std::cout << "Expecting size 4, actual size. ";
	std::cout << Vtemp << std::endl;

	Pop_Front();
	std::cout << "Removing first value" << std::endl;

	Pop_Back();
	std::cout << "Removing last value" << std::endl;

	Vtemp = GetSize();
	std::cout << "Expecting size 2, actual size. ";
	std::cout << Vtemp << std::endl;

	Vtemp = FindNodeWithPosition(2);
	std::cout << "Finding node whit position 2 expecting value 3, actual value: ";
	std::cout << Vtemp << std::endl;

	ClearNodes();
	std::cout << "Clearing All nodes" << std::endl;

	Vtemp = GetSize();
	std::cout << "Checking size of list expecting value 0, actual value: ";
	std::cout << Vtemp << std::endl;

	std::cout << "This concludes the test." << std::endl;
}

int main(int argc, char* argv[])
{
	SHeadNode->value = NULL;
	SHeadNode->Nextnode = NULL;

	SCurrNode->value = NULL;
	SCurrNode->Nextnode = NULL;

	STempNode->value = NULL;
	STempNode->Nextnode = NULL;

	std::string WritenInput;
	int value;
	bool running = false;

	std::cout << "Final Version of linked-list testing" << std::endl;
	std::cout << "Type test to run Uni testing" << std::endl;
	std::cout << "Valid Comands are: pushfront , pushback , popfront , popback , find and clear" << std::endl;
	std::cout << "type end to quit." << std::endl;


	while (!running)
	{
		std::cin >> WritenInput;

		if (WritenInput == "test")
		{
			UniTest();
		}

		if (WritenInput == "pushfront")
		{
			std::cout << "Type value to be pushed." << std::endl;
			std::cin >> value;
			Push_Front(value);
			std::cout << "Value Has Been Bushed To The Front.." << std::endl;
		}

		if (WritenInput == "pushback")
		{
			std::cout << "Type value to be pushed." << std::endl;
			std::cin >> value;
			Push_Back(value);
			std::cout << "Value Has Been Bushed To The Back." << std::endl;
		}

		if (WritenInput == "size")
		{
			int tempV = GetSize();
			std::cout << tempV << " : Nodes in the list." << std::endl;
		}

		if (WritenInput == "find")
		{
			std::cout << "Type Possition to find." << std::endl;
			std::cin >> value;
			int tempV = FindNodeWithPosition(value);
			if (tempV == INT_MAX)
			{
				std::cout << value << " : Node dos not exist. There are only " << GetSize() << " :Nodes in the list." << std::endl;
			}
			else
			{
				std::cout << value << " : Node contains value: " << tempV << std::endl;
			}
		}

		if (WritenInput == "popfront")
		{
			Pop_Front();
			std::cout << "Front node has been deleted" << std::endl;
		}

		if (WritenInput == "popback")
		{
			Pop_Back();
			std::cout << "Last node has been deleted" << std::endl;
		}

		if (WritenInput == "clear")
		{
			ClearNodes();
			std::cout << "Nodes Have been removed." << std::endl;
		}

		if (WritenInput == "end")
		{
			running = true;
		}
	}

}

