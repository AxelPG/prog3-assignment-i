
#include "stdafx.h"

struct StructNodeBase
{
	int value;
	StructNodeBase* NextLow;
	StructNodeBase* NextHigh;
};

StructNodeBase* SHeadNode = new StructNodeBase;
StructNodeBase* SCurrNode = new StructNodeBase;
StructNodeBase* STempNode = new StructNodeBase;

void Insert(int value)
{
	int state = true;
	StructNodeBase* SNewNode = new StructNodeBase;

	SNewNode->value = value;
	SNewNode->NextLow = nullptr;
	SNewNode->NextHigh = nullptr;

	if (SHeadNode == NULL || SHeadNode->value == NULL)
	{
		SHeadNode = SNewNode;
		state = false;
		return;

	}

	SCurrNode = SHeadNode;

	while (state)
	{
		if (SCurrNode->value == value)
		{
			std::cout << "Value already exist in the tree please type another" << std::endl;
			state = false;
			break;
		}
		if (SNewNode->value > SCurrNode->value)
		{
			if (SCurrNode->NextHigh != nullptr)
			{
				SCurrNode = SCurrNode->NextHigh;
			}
			else
			{
				SCurrNode->NextHigh = SNewNode;
				state = false;
			}
		}

		if (SNewNode->value < SCurrNode->value)
		{
			if (SCurrNode->NextLow != nullptr)
			{
				SCurrNode = SCurrNode->NextLow;
			}
			else
			{
				SCurrNode->NextLow = SNewNode;
				state = false;
			}
		}
	}
}

StructNodeBase* FindNode(int value, StructNodeBase* node)
{
	if (node != NULL)
	{
		if (node->value == value)
		{
			return node;
		}
		else
		{
			if (value < node->value)
			{
				return FindNode(value, node->NextLow);
			}
			else
			{
				return FindNode(value, node->NextHigh);
			}
		}
	}
	else
	{
		return NULL;
	}
}

void ClearNodes(StructNodeBase* node)
{
	if (node != NULL)
	{
		if (node->NextLow != NULL)
		{
			ClearNodes(node->NextLow);
		}

		if (node->NextHigh != NULL)
		{
			ClearNodes(node->NextHigh);
		}

		if (node->value == SHeadNode->value)
		{
			SHeadNode = nullptr;
		}

		SCurrNode = node;
		delete SCurrNode;
	}
	else
	{
		std::cout << "There are  already no nodes in the tree" << std::endl;
	}
}

void TraversalPreOrder(StructNodeBase* node)
{
	if (node != NULL)
	{
		std::cout << node->value << " ,";

		if (node->NextLow != NULL)
		{
			TraversalPreOrder(node->NextLow);
		}
		if (node->NextHigh != NULL)
		{
			TraversalPreOrder(node->NextHigh);
		}
	}
	else
	{
		std::cout << "There are no nodes in the tree" << std::endl;
	}
}

void TraversalInOrder(StructNodeBase* node)
{
	if (node != NULL)
	{
		if (node->NextLow != NULL)
		{
			TraversalInOrder(node->NextLow);
		}

		std::cout << node->value << " ,";

		if (node->NextHigh != NULL)
		{
			TraversalInOrder(node->NextHigh);
		}
	}
	else
	{
		std::cout << "There are no nodes in the tree" << std::endl;
	}
}

void TraversalPostOrder(StructNodeBase* node)
{
	if (node != NULL)
	{
		if (node->NextLow != NULL)
		{
			TraversalPostOrder(node->NextLow);
		}
		
		if (node->NextHigh != NULL)
		{
			TraversalPostOrder(node->NextHigh);
		}

		std::cout << node->value << " ,";
	}
	else
	{
		std::cout << "There are no nodes in the tree" << std::endl;
	}
}

void UniTest()
{
	std::cout << "Running Uni test, Inserting Folowing nubers." << std::endl;
	std::cout << "8, 3, 10, 1, 6, 14" << std::endl;
	std::cout << "\n";
	Insert(8);
	Insert(3);
	Insert(10);
	Insert(1);
	Insert(6);
	Insert(14);

	std::cout << "Traversing tree in Order Expecting: 1, 3, 6, 8, 10 ,14.\nRecieved the following: ";
	TraversalInOrder(SHeadNode);
	std::cout << "\n\n";

	std::cout << "Traversing tree in Pre-order Expecting: 8, 3, 1, 6, 10 ,14.\nRecieved the following: " ;
	TraversalPreOrder(SHeadNode);
	std::cout << "\n\n";

	std::cout << "Traversing tree in Post-order Expecting: 1, 6, 3, 14, 10 ,8.\nRecieved the following: ";
	TraversalPostOrder(SHeadNode);
	std::cout << "\n\n";

	SCurrNode = FindNode(3, SHeadNode);
	std::cout << "Finding node with value. Expecting 3 Finds: " << SCurrNode->value << std::endl;

	std::cout << "Clearing nodes From list" << std::endl;
	ClearNodes(SHeadNode);

	std::cout << "Traversing Tree in order Expecting: There are no nodes in the tree, Get: " << std::endl;
	TraversalInOrder(SHeadNode);
	std::cout << "\n";

	std::cout << "This concludes The uniTest for this binary search tree." << std::endl;
}

int main(int argc, char* argv[])
{
	SHeadNode->value = NULL;
	SHeadNode->NextLow = NULL;
	SHeadNode->NextHigh = NULL;

	SCurrNode->value = NULL;
	SCurrNode->NextLow = NULL;
	SCurrNode->NextHigh = NULL;

	STempNode->value = NULL;
	STempNode->NextLow = NULL;
	STempNode->NextHigh = NULL;


	std::vector<int> VisitedNodes;
	std::string WritenInput;
	int value;
	bool running = false;

	std::cout << "Final Version of linked-list testing" << std::endl;
	std::cout << "Type test to run Uni testing" << std::endl;
	std::cout << "Valid Comands are: insert , find , traversalpreorder , traversalinorder ,\ntraversalpostorder and clear" << std::endl;
	std::cout << "type end to quit." << std::endl;


	while (!running)
	{
		std::cin >> WritenInput;

		if (WritenInput == "mass")
		{
			Insert(8);
			Insert(3);
			Insert(10);
			Insert(1);
			Insert(6);
			Insert(14);
		}

		if (WritenInput == "test")
		{
			UniTest();
		}

		if (WritenInput == "insert")
		{
			std::cout << "Type Value to insert." << std::endl;
			std::cin >> value;
			Insert(value);
		}

		if (WritenInput == "find")
		{
			std::cout << "Type value to find." << std::endl;
			std::cin >> value;
			SCurrNode = FindNode(value, SHeadNode);
			if (SCurrNode != NULL)
			{
				std::cout << "found node with value: " << SCurrNode->value << std::endl;
			}
			else
			{
				std::cout << "Could not find node whit value" << std::endl;
			}
		}

		if (WritenInput == "traversalpreorder")
		{
			TraversalPreOrder(SHeadNode);
			std::cout << "\n";
		}

		if (WritenInput == "traversalinorder")
		{
			TraversalInOrder(SHeadNode);
			std::cout << "\n";
		}

		if (WritenInput == "traversalpostorder")
		{
			TraversalPostOrder(SHeadNode);
			std::cout << "\n";
		}

		if (WritenInput == "clear")
		{
			ClearNodes(SHeadNode);
			std::cout << "Nodes Have been removed." << std::endl;
		}

		if (WritenInput == "end")
		{
			running = true;
		}
	}

}
